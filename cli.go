package main

import (
	"fmt"
	"github.com/docopt/docopt-go"
	"os"
)

var usage = `esrv

Usage:
  esrv [--port=<INT>] [--log-file=<PATH>]
  esrv -h | --help
  esrv --version

Options:
  --port=<INT>       Server's port. [default: 8081]
  --log-file=<PATH>  File logs are written to. [default: esrv.log.txt]
  -h --help          Show this help message.
  --version          Show version.

`

type CliArgs struct {
	Port    int    `docopt:"--port"`
	LogFile string `docopt:"--log-file"`
}

var Args = CliArgs{}

func init() {
	argv := os.Args[1:]
	opts, err := docopt.ParseArgs(usage, argv, "0.1.0")
	if err != nil {
		fmt.Println(err)
	}

	opts.Bind(&Args)
}
