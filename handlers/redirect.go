package handlers

import (
	"gitlab.com/mgemmill/esrv/utils"
	"net/http"
	"net/url"
)

var RedirectHandler = NewHttpHandler(
	[]string{"POST", "GET"},
	[]string{"url"},
	func(w http.ResponseWriter, req *http.Request) {
		irl, _ := url.Parse(req.RequestURI)
		parameters := irl.Query()
		redirectTo := fetchFirst(parameters["url"])
		utils.Echo("Redirecting to: %s\n", redirectTo)
		http.Redirect(w, req, redirectTo, http.StatusTemporaryRedirect)
	},
)
