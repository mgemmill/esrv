package handlers

import (
	"bytes"
	"fmt"
	"gitlab.com/mgemmill/esrv/utils"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

func callUrl(flags UrlFlags, body string) string {
	var sb strings.Builder
	var res *http.Response
	var err error

	fmt.Fprintf(&sb, "   calling %s %s\n", flags.callMethod, flags.url)

	if flags.callMethod == "POST" {
		res, err = http.Post(flags.url, "application/text", bytes.NewBufferString(body))
	} else {
		res, err = http.Get(flags.url)
	}

	if err != nil {
		fmt.Fprintf(&sb, "   failed to call GET %s\n", flags.url)
		fmt.Fprintf(&sb, "   %s\n", err)
	} else {
		response, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			fmt.Fprintf(&sb, "   failed to read response body...\n")
			fmt.Fprintf(&sb, "   %s\n", err)
		}

		fmt.Fprintf(&sb, "<<<<<< RESPONSE START >>>>>>\n")
		fmt.Fprintf(&sb, "%s\n", response)
		fmt.Fprintf(&sb, "<<<<<< RESPONSE END >>>>>>\n")
	}

	return sb.String()

}

var EchoHandler = NewHttpHandler(
	[]string{"POST", "GET"},
	[]string{},
	func(w http.ResponseWriter, req *http.Request) {
		var sb strings.Builder

		// parsing the request
		irl, err := url.Parse(req.RequestURI)
		if err != nil {
			fmt.Sprintln(&sb, "\nError parsing query string.", err)
		}

		params := irl.Query()
		urlFlags := NewUrlFlags(irl.Query())

		content, err := ioutil.ReadAll(req.Body)
		if err != nil {
			fmt.Fprintf(&sb, "\nError reading body: %v", err)
		}

		// Writing Output
		fmt.Fprintln(&sb, displayRequestInfo(req, params, content))

		// Actions
		if urlFlags.callURL && urlFlags.url != "" {
			fmt.Fprintln(&sb, "\nActions:")
		}

		if urlFlags.callURL && urlFlags.url != "" {
			callResults := callUrl(urlFlags, "")
			fmt.Fprintln(&sb, callResults)
		}

		utils.Echo(sb.String())

		// Response
		w.WriteHeader(http.StatusOK)

		switch req.Method {
		case "GET":
			io.WriteString(w, sb.String())
		case "POST":
			fmt.Println(sb.String())
			io.WriteString(w, "OK")
		default:
			io.WriteString(w, fmt.Sprintf("OK - but unhandled method %s", req.Method))
		}
	},
)
