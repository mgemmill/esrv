package handlers

import (
	"fmt"
	"net/http"
)

//HomeHandler
var HomeHandler = NewHttpHandler(
	[]string{"GET"},
	[]string{},
	func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintln(w, "Welcome to Echo Server.\n\n")
		fmt.Fprintln(w, "Usage:\n")
		fmt.Fprintln(w, "  localhost:8081/api")
		fmt.Fprintln(w, "  - echoes http request info to browser, console and log file.\n")
		fmt.Fprintln(w, "  localhost:8081/api/redirect?url=http://google.com")
		fmt.Fprintln(w, "  - redirects to the given 'url' parameter, otherwise echoes error to browser.\n")
		fmt.Fprintln(w, "")
	},
)
