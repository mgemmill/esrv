package handlers

import (
	"strings"
)

type UrlFlags struct {
	id          string
	saveRawData bool
	callURL     bool
	callMethod  string
	response    string
	url         string
}

func cBool(s string) bool {
	if s == "yes" {
		return true
	}
	return false
}

func fetchFirst(param []string) string {
	if len(param) == 0 {
		return ""
	}
	return param[0]
}

func setDefault(value string, def string) string {
	if value == "" {
		return def
	}
	return value
}

func NewUrlFlags(parms map[string][]string) UrlFlags {
	p := UrlFlags{}
	p.id = fetchFirst(parms["id"])
	p.saveRawData = cBool(fetchFirst(parms["save-raw-data"]))
	p.callURL = cBool(fetchFirst(parms["call-url"]))
	p.callMethod = strings.ToUpper(setDefault(fetchFirst(parms["call-method"]), "GET"))
	p.response = fetchFirst(parms["response"])

	p.url = fetchFirst(parms["url"])
	return p
}
