package handlers

import (
	"fmt"
	"net/http"
	"strings"
	"time"
)

// RequestHeaders returns html headers in pretty format.
func RequestHeaders(sb *strings.Builder, req *http.Request) {

	fmt.Fprintf(sb, "\nRequest:\n")
	fmt.Fprintf(sb, "  Remote Addr: %s\n", req.RemoteAddr)
	fmt.Fprintf(sb, "  Request URI: %s%s\n", req.Host, req.RequestURI)
	fmt.Fprintf(sb, "  Protocol: %s\n", req.Proto)
	fmt.Fprintf(sb, "  Method: %s\n", req.Method)

	fmt.Fprintf(sb, "\nHeaders:\n")
	for key, val := range req.Header {
		for i, mval := range val {
			keyLen := len(key)
			if i == 0 {
				fmt.Fprintf(sb, "   %s: %s\n", key, mval)
			} else {
				fmt.Fprintf(sb, "   %s  %s\n", strings.Repeat(" ", keyLen), mval)
			}
		}
	}

}

// RequestURLParameters - returns URL parameters in pretty format.
func RequestURLParameters(sb *strings.Builder, params map[string][]string) {

	fmt.Fprintln(sb, "\nQuery Parameters:")
	for key, val := range params {
		fmt.Fprintf(sb, "   %s: %s\n", key, val[0])
	}
}

// displayRequestInfo writes request headers, parameters and content to
// the request response.
func displayRequestInfo(req *http.Request, params map[string][]string, content []byte) string {

	var sb strings.Builder

	now := time.Now()

	// Writing Output
	fmt.Fprintln(&sb, "")
	fmt.Fprintln(&sb, strings.Repeat("-", 60))
	fmt.Fprintln(&sb, now.Format(time.RFC3339))

	RequestHeaders(&sb, req)
	RequestURLParameters(&sb, params)

	fmt.Fprintf(&sb, "\nContent Length: %d\n", req.ContentLength)

	if len(content) > 0 {
		fmt.Fprintln(&sb, "\nContent:\n")
		fmt.Fprintln(&sb, "CONTENT START >>>>>")
		fmt.Fprintf(&sb, "%s\n", content)
		fmt.Fprintln(&sb, "<<<<< CONTENT END")
	}

	return sb.String()

}
