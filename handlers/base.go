package handlers

import (
	"fmt"
	"net/http"
	"net/url"
)

// CheckMethod is a decorator that filters request methods.
func CheckMethod(inner http.HandlerFunc, acceptedMethods []string) http.HandlerFunc {

	if len(acceptedMethods) == 0 {
		return inner
	}

	return func(w http.ResponseWriter, req *http.Request) {
		validMethod := false
		for _, m := range acceptedMethods {
			if m == req.Method {
				validMethod = true
			}
		}

		if validMethod == true {
			inner(w, req)
		} else {
			http.Error(w, fmt.Sprintf("%s is an invalid method.", req.Method), http.StatusMethodNotAllowed)
		}
	}
}

// CheckParameters is a decorator that filters for request URL paramters.
func CheckParameters(inner http.HandlerFunc, requiredParams []string) http.HandlerFunc {
	if len(requiredParams) == 0 {
		return inner
	}
	return func(w http.ResponseWriter, req *http.Request) {

		// TODO: skipping err here....
		irl, _ := url.Parse(req.RequestURI)
		parameters := irl.Query()

		missingParams := false
		// checking for required parameters
		for _, required := range requiredParams {
			fmt.Printf("checking for %s param...", required)
			currentExists := false
			for k, _ := range parameters {
				if k == required {
					currentExists = true
					break
				}
			}
			if !currentExists {
				missingParams = true
				break
			}
		}

		if !missingParams {
			inner(w, req)
		} else {
			http.Error(w, "Missing URL parameters.", http.StatusNotAcceptable)
		}
	}
}

// HttpHandler constructor
func NewHttpHandler(methods []string, parameters []string, handler http.HandlerFunc) http.HandlerFunc {
	return CheckParameters(CheckMethod(handler, methods), parameters)
}
