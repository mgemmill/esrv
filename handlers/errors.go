package handlers

type Error interface {
	error
	Status() int
}

type HandlerError struct {
	Code int
	Err  error
}

func (e HandlerError) Error() string {
	return e.Err.Error()
}

func (e HandlerError) Status() int {
	return e.Code
}

func NewHandlerError(status int, err error) HandlerError {
	return HandlerError{
		Code: status,
		Err:  err,
	}
}
