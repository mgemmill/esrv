package utils

import (
	"fmt"
	"log"
	"os"
)

var LogFile *os.File

func InitializeLogging(logFile string) {
	// TODO: check that logFile is a real path
	LogFile, err := os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("Error opening log file: %v", err)
	}

	log.SetOutput(LogFile)
}

func Echo(msg string, a ...interface{}) {
	fmt.Printf(msg, a...)
	log.Printf(msg, a...)
}
