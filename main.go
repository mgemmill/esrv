package main

import (
	"fmt"
	"gitlab.com/mgemmill/esrv/handlers"
	"gitlab.com/mgemmill/esrv/utils"
	"log"
	"net/http"
)

func main() {

	utils.InitializeLogging(Args.LogFile)
	defer utils.LogFile.Close()

	http.Handle("/api/redirect", handlers.RedirectHandler)
	http.Handle("/api", handlers.EchoHandler)
	http.Handle("/", handlers.HomeHandler)

	url := fmt.Sprintf("127.0.0.1:%d", Args.Port)

	utils.Echo("Started Echo server on %s\n", url)

	log.Fatal(http.ListenAndServe(url, nil))
}
